package com.jhunter.jpmc_one

import com.jhunter.jpmc_one.service.RestInterface
import com.jhunter.jpmc_one.service.ServiceGenerator
import com.jhunter.jpmc_one.ui.ItemDetailActivity
import com.jhunter.jpmc_one.ui.ItemListActivity
import org.junit.Assert.assertNotNull
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class JpmcUnitTest {

    @Test
    fun activities_shouldNotBeNull() {
        val itemListActivity = ItemListActivity()
        assertNotNull(itemListActivity)

        val itemDetailActivity = ItemDetailActivity()
        assertNotNull(itemDetailActivity)
    }

    @Test
    fun serviceGenerator_shouldNotBeNull() {
        val serviceGenerator: RestInterface =
            ServiceGenerator.createService(RestInterface::class.java)
        assertNotNull(serviceGenerator)
    }
}
