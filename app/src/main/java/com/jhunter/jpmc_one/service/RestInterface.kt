package com.jhunter.jpmc_one.service

import com.jhunter.jpmc_one.model.school.HighSchool
import retrofit2.Call
import retrofit2.http.GET

// Definition of REST endpoints
interface RestInterface {

    @GET("uq7m-95z8.json")
    fun getHighSchools(): Call<List<HighSchool>>

}