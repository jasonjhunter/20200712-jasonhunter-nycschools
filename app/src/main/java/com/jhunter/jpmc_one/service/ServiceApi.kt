package com.jhunter.jpmc_one.service

import com.jhunter.jpmc_one.BuildConfig
import com.jhunter.jpmc_one.model.school.HighSchool
import com.jhunter.jpmc_one.model.service.Resource
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Response
import timber.log.Timber

val serviceApiModule: Module = module { single { ServiceApi(get()) } }

// Note: In a real world scenario, there would be a lot more
// error catching/handling in this class and throughout the entire app
class ServiceApi(private val responseHandler: ResponseHandler) {

    fun getHighSchools(): Resource<List<HighSchool>> {
        Timber.d("getHighSchools")

        // TODO: introduce more robust error handling
        try {

            val response: Response<List<HighSchool>> =
                restInterface.getHighSchools().execute()

            if (response.errorBody() != null) {
                throw Exception(response.raw().toString())
            } else {
                var responseBody = response.body()

                Timber.d(
                    "\nresponse code : ${response.code()}\n" +
                            "response body : ${response.body()}"
                )

                return responseHandler.handleSuccess(responseBody!!)
            }
        } catch (e: Exception) {

            Timber.e(e)

            return responseHandler.handleException(e)
        }
    }


    companion object {

        private var hostUrl: String = BuildConfig.API_URL

        fun getHostUrl(): String {
            return hostUrl
        }

        val restInterface: RestInterface
            get() {
                return ServiceGenerator.createService(RestInterface::class.java)
            }
    }
}
