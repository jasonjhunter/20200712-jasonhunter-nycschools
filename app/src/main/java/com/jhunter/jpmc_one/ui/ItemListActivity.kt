package com.jhunter.jpmc_one.ui

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.jhunter.jpmc_one.R
import com.jhunter.jpmc_one.Status
import com.jhunter.jpmc_one.model.school.HighSchool
import com.jhunter.jpmc_one.model.service.Resource
import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.item_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * An activity representing a list of NYC High Schools. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of schools, which when touched,
 * lead to a [ItemDetailActivity] representing
 * school details. On tablets, the activity presents the list of schools and
 * school details side-by-side using two vertical panes.
 */
class ItemListActivity : BaseActivity() {

    //  ******************** ViewModel initialization ********************
    private val itemViewModel: ItemViewModel by viewModel()

    //  ******************** Observer initialization ********************
    private val highSchoolListObserver: Observer<Resource<List<HighSchool>>> =
        Observer {
            when (it.status) {
                Status.SUCCESS -> setupRecyclerView(it.data)
                Status.ERROR -> showError(
                    getString(R.string.jpmc_error, it.message), progressbar_loading
                )
                Status.LOADING -> showLoading(getString(R.string.jpmc_loading), progressbar_loading)
            }
        }

    private val errorObserver: Observer<Exception> =
        Observer {
            Toast.makeText(this, it.localizedMessage, Toast.LENGTH_LONG).show()
        }

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (item_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        itemViewModel.getHighSchoolsResponse.observe(this, highSchoolListObserver)
        itemViewModel.errorMessage.observe(this, errorObserver)
        itemViewModel.getHighSchools("optional_param_value")
        showLoading(getString(R.string.jpmc_loading), progressbar_loading)
    }

    private fun setupRecyclerView(list: List<HighSchool>?) {

        hideLoading(progressbar_loading)

        if (list != null) {
            if (list.isNotEmpty()) {
                item_list.adapter =
                    ItemRecyclerViewAdapter(
                        this,
                        list,
                        twoPane
                    )
            }
        }
    }

}
