package com.jhunter.jpmc_one.ui

import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {

    protected fun showLoading(
        message: String,
        progressBar: ProgressBar
    ) {
        progressBar.visibility = View.VISIBLE
    }

    protected fun hideLoading(progressBar: ProgressBar) {
        progressBar.visibility = View.GONE
    }

    protected fun showError(
        message: String,
        progressBar: ProgressBar
    ) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        hideLoading(progressBar)
    }
}