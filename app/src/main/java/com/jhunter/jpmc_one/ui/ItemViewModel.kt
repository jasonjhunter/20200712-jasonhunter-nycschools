package com.jhunter.jpmc_one.ui

import androidx.lifecycle.*
import com.jhunter.jpmc_one.model.school.HighSchool
import com.jhunter.jpmc_one.model.service.Resource
import com.jhunter.jpmc_one.service.ServiceApi
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module
import timber.log.Timber

val itemViewModelModule = module { factory { ItemViewModel(get()) } }

class ItemViewModel(private val serviceApi: ServiceApi) : ViewModel() {

    private val _errorMessage = MutableLiveData<Exception>()
    val errorMessage: LiveData<Exception> = _errorMessage

    private val _getHighSchools = MutableLiveData<String>()

    fun getHighSchools(input: String) {
        _getHighSchools.value = input  // Trigger the value change
    }

    var getHighSchoolsResponse: LiveData<Resource<List<HighSchool>>> =
        _getHighSchools.switchMap() { _ ->
            liveData(Dispatchers.IO) {
                emit(Resource.loading(null))          // First emits loading status
                try {
                    emit(serviceApi.getHighSchools())  // After the API returns a value, it will be emitted to the the Fragment
                } catch (e: Exception) {
                    Timber.e(e)
                    _errorMessage.postValue(e)
                }
            }
        }
}