package com.jhunter.jpmc_one.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jhunter.jpmc_one.R
import com.jhunter.jpmc_one.model.school.HighSchool
import kotlinx.android.synthetic.main.item_detail.view.*

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a [ItemListActivity]
 * in two-pane mode (on tablets) or a [ItemDetailActivity]
 * on handsets.
 */
class ItemDetailFragment : Fragment() {

    /**
     * The school content this fragment is presenting.
     */
    private var item: HighSchool? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM)) {
                // Load the school content specified by the fragment arguments.
                item = it.getSerializable(ARG_ITEM) as? HighSchool
                activity?.setTitle(item?.school_name)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.item_detail, container, false)

        // Show the school content as text in in TextViews.
        item?.let {
            rootView.item_detail.text = "${it.overview_paragraph} \n\n ${it.neighborhood}"
        }

        return rootView
    }

    companion object {
        const val ARG_ITEM = "item"
    }
}
