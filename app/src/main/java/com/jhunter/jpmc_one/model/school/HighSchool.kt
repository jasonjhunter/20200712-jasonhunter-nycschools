package com.jhunter.jpmc_one.model.school

import com.google.gson.annotations.SerializedName

// This data class does not add any additional methods/properties
// its job is simply to implement the properties in its Interface
// SOLID #4 - Interface Segregation principle (ISP)
data class HighSchool(

    @SerializedName("dbn")
    override var dbn: String,

    @SerializedName("neighborhood")
    override var neighborhood: String,

    @SerializedName("campus_name")
    override var campus_name: String,

    @SerializedName("boro")
    override var boro: String,

    @SerializedName("school_name")
    val school_name: String,

    @SerializedName("overview_paragraph")
    val overview_paragraph: String,

    @SerializedName("academicopportunities1")
    val academicopportunities1: String,

    @SerializedName("college_career_rate")
    override val college_career_rate: String,

    @SerializedName("pct_stu_safe")
    override val pct_stu_safe: String

) : IHighSchool