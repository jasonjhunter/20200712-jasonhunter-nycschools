package com.jhunter.jpmc_one.model.school

// This Interface class inherits the IBaseSchool Interface
// and adds one additional property
interface IHighSchool : IBaseSchool {
    val college_career_rate: String
}