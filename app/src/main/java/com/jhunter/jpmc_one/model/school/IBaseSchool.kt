package com.jhunter.jpmc_one.model.school

import java.io.Serializable

// Base school class intended to be implemented by most school classes (IE. elementary, high School, etc.)
interface IBaseSchool : Serializable {
    val dbn: String
    var neighborhood: String
    var campus_name: String
    var boro: String
    val pct_stu_safe: String
}