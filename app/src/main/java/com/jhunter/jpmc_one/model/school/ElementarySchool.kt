package com.jhunter.jpmc_one.model.school

// An example of another class that 'could' also implement the IBaseSchool Interface.
// For illustration purposes only.  Not currently used.
class ElementarySchool(
    override val dbn: String,
    override var neighborhood: String,
    override var campus_name: String,
    override var boro: String,
    override val pct_stu_safe: String
) : IBaseSchool