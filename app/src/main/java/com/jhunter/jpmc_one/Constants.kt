package com.jhunter.jpmc_one

/**
 * Constants used throughout the app.
 */
const val NYC_API_HS_URI = "https://data.cityofnewyork.us/resource/uq7m-95z8.json"

enum class Status {
    SUCCESS,
    ERROR,
    SERVICE_ERROR,
    LOADING
}