package com.jhunter.jpmc_one

import android.app.Application
import android.content.Context
import android.util.Log
import com.jhunter.jpmc_one.service.responseHandlerModule
import com.jhunter.jpmc_one.service.serviceApiModule
import com.jhunter.jpmc_one.ui.itemViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber


class App : Application() {

    private val instance: App = this.getInstance()

    override fun onCreate() {
        super.onCreate()

        Log.i(TAG, "App initialization")

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree());
        } else {
            //Timber.plant(CrashReportingTree());
        }

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(
                listOf(
                    itemViewModelModule,      // HighSchoolListViewModel
                    responseHandlerModule,      // responseHandlerModule
                    serviceApiModule          // ServiceApi
                )
            )
        }

        Companion.appContext = applicationContext

    }

    fun getInstance(): App {
        return instance
    }

    companion object {

        private val TAG = App::class.java.simpleName

        private lateinit var appContext: Context

        fun getAppContext(): Context {
            return appContext
        }
    }
}